import PropTypes from 'prop-types';
import React from 'react';
import TopPersonsModel from '../../repository/model/TopPersons';
import TopPerson from './TopPerson';

export default function TopPersons(props) {
    const { topPersons, isLoading } = props;

    if (true === isLoading) {
        return (<div className="text-center py-2">Chargement...</div>);
    }

    return (
        <div>
            <div className="flex flex-row flex-wrap -mx-2">
                {topPersons.list.map((topPerson) => (
                    <div key={topPerson.personId} className="w-1/5 p-2">
                        <TopPerson topPerson={topPerson} />
                    </div>
                ))}
            </div>
        </div>
    );
};

TopPersons.propTypes = {
    topPersons: PropTypes.instanceOf(TopPersonsModel).isRequired,
    isLoading:  PropTypes.bool.isRequired,
};
