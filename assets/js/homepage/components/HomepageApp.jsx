import React, {
    useEffect,
    useState,
} from 'react';
import Heading from '../../components/Heading';
import TopPersonsModel from '../../repository/model/TopPersons';
import topPersonRepository from '../../repository/TopPersonRepository';
import Filters from './Filters';
import TopPersons from './TopPersons';

export default function HomepageApp() {
    const [topPersons, setTopPersons] = useState(TopPersonsModel.empty());
    const [isLoading, setIsLoading]   = useState(false);
    const [nameFilter, setNameFilter] = useState('');

    const topCount = 10;

    useEffect(() => {
        setIsLoading(true);

        topPersonRepository.list(topCount).then((topPersons) => {
            setTopPersons(topPersons);
            setIsLoading(false);
        });
    }, []);

    return (
        <div className="container mx-auto">
            <Heading>{`Classement des ${topCount} meilleurs joueurs :`}</Heading>
            <Filters nameFilter={nameFilter} setNameFilter={setNameFilter} />
            <TopPersons
                topPersons={topPersons.search(nameFilter)}
                isLoading={isLoading}
            />
        </div>
    );
}

HomepageApp.propTypes = {};
