import '@fortawesome/fontawesome-free/js/all';
import 'whatwg-fetch';

const navContent = document.getElementById('nav-content');

document.getElementById('nav-toggle').addEventListener('click', () => {
  navContent.classList.toggle('hidden');
});
