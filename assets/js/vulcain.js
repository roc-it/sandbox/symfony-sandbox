let requestOptions = {
    method:   'GET',
    headers:  {
        'Preload':      '/persons/*/_links/tetris_games',
        'Content-Type': 'application/json',
    },
    redirect: 'follow',
};

fetch('/api/persons', requestOptions)
    .then(response => {
        return response.json();
    })
    .then(result => {
        console.log(result);

        return Promise.all(result.persons.map((person) => {
            return fetch(person._links.tetris_games, {
                method:   'GET',
                redirect: 'follow',
            });
        }));
    })
    .then(results => {
        results.forEach((result) => {
            result.json()
                  .then(result => console.log(result))
            ;
        });
    })
    .catch(error => console.log('error', error));
