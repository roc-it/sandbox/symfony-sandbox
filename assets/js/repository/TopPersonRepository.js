'use strict';

import TopPersons from './model/TopPersons';

class TopPersonRepository {
    /**
     * @param {number|null} [limit]
     *
     * @returns {Promise<TopPersons>}
     */
    async list(limit = null) {
        const url = new URL(`/api/top-persons`, window.location.href);

        if (null !== (limit ?? null)) {
            url.searchParams.set('limit', limit);
        }

        const headers = new Headers();

        headers.append('Accept', 'application/json');

        const request = new Request(url, {
            'method':  'GET',
            'headers': headers,
        });

        return fetch(request)
            .then((response) => {
                return response.json();
            })
            .then((json) => {
                return TopPersons.parse(json);
            });
    }
}

const topPersonRepository = new TopPersonRepository();

export default topPersonRepository;
