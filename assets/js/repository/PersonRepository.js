'use strict';

import Persons from './model/Persons';

class PersonRepository {
    /**
     * @returns {Promise<Persons>}
     */
    async list() {
        return fetch(`/api/persons`)
            .then((response) => {
                return response.json();
            })
            .then((json) => {
                return Persons.parse(json);
            });
    }
}

const personRepository = new PersonRepository();

export default personRepository;
