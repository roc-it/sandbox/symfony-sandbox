'use strict';

import Fuse from 'fuse.js';
import TopPerson from './TopPersons/TopPerson';

class TopPersons {
    /** @var {number} */
    #total;
    /** @var {Array.<TopPerson>} */
    #list;
    /** @var {string} */
    #filter;
    /** @var {Fuse} */
    #fuse;

    /**
     * @param {number} total
     * @param {Array.<TopPerson>} list
     * @param {string} [filter]
     */
    constructor(total, list, filter = '') {
        this.#total  = total;
        this.#list   = list;
        this.#filter = filter;
        this.#fuse   = new Fuse(list, {
            includeScore:       false,
            useExtendedSearch:  false,
            minMatchCharLength: 1,
            shouldSort:         true,
            includeMatches:     false,
            keys:               [
                'personName',
            ],
            sortFn:             (a, b) => {
                const { itemLeft, itemRight } = { itemLeft: this.#list[a.idx], itemRight: this.#list[b.idx] };

                return TopPersons.#sortList(itemLeft, itemRight);
            },
        });
    }

    /**
     * @returns {TopPersons}
     */
    static empty() {
        return new TopPersons(0, []);
    }

    /**
     * @param raw
     * @returns {TopPersons}
     */
    static parse(raw) {
        return new TopPersons(
            raw.length,
            raw.map((topPerson) => TopPerson.parse(topPerson)),
        );
    }

    /**
     * @returns {number}
     */
    get total() {
        return this.#total;
    }

    /**
     * @returns {Array.<TopPerson>}
     */
    get list() {
        if (0 === this.#filter.length) {
            return this.#list.sort(TopPersons.#sortList.bind(this));
        }

        return this.#fuse.search(this.#filter).map((finding) => {
            return finding.item;
        });
    }

    /**
     * @param {TopPerson} itemLeft
     * @param {TopPerson} itemRight
     * @returns {number}
     */
    static #sortList(itemLeft, itemRight) {
        if (null === itemLeft.averageRank) {
            return +1;
        }

        if (null === itemRight.averageRank) {
            return -1;
        }

        if (itemLeft.averageRank !== itemRight.averageRank) {
            return itemLeft.averageRank.toLocaleString().localeCompare(itemRight.averageRank.toLocaleString());
        }

        if (itemLeft.gameCount !== itemRight.gameCount) {
            return itemRight.gameCount.toLocaleString().localeCompare(itemLeft.gameCount.toLocaleString());
        }

        return itemRight.victoryCount.toLocaleString().localeCompare(itemLeft.victoryCount.toLocaleString());
    }

    /**
     * @param {string} filter
     * @returns {TopPersons}
     */
    search(filter) {
        return new TopPersons(
            this.total,
            this.list,
            filter,
        );
    }

    /**
     * @param {string} id
     * @returns {TopPerson}
     */
    fetchById(id) {
        return this.#list.find((topPerson) => {
            return topPerson.personId === id;
        });
    }
}

export default TopPersons;
