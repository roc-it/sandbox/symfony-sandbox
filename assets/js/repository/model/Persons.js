'use strict';

import Fuse from 'fuse.js';
import Person from './Persons/Person';

class Persons {
    /** @var {number} */
    #total;
    /** @var {Array.<Person>} */
    #list;
    /** @var {string} */
    #filter;
    /** @var {Fuse} */
    #fuse;

    /**
     * @param {number} total
     * @param {Array.<Person>} list
     * @param {string} [filter]
     */
    constructor(total, list, filter = '') {
        this.#total  = total;
        this.#list   = list;
        this.#filter = filter;
        this.#fuse   = new Fuse(list, {
            includeScore:       false,
            useExtendedSearch:  false,
            minMatchCharLength: 1,
            shouldSort:         true,
            includeMatches:     false,
            keys:               [
                'name',
            ],
            sortFn:             (a, b) => {
                const { itemLeft, itemRight } = { itemLeft: this.#list[a.idx], itemRight: this.#list[b.idx] };

                return Persons.#sortList(itemLeft, itemRight);
            },
        });
    }

    /**
     * @returns {Persons}
     */
    static empty() {
        return new Persons(0, []);
    }

    /**
     * @param raw
     * @returns {Persons}
     */
    static parse(raw) {
        return new Persons(
            raw.count,
            raw.persons.map((person) => Person.parse(person)),
        );
    }

    /**
     * @returns {number}
     */
    get total() {
        return this.#total;
    }

    /**
     * @returns {Array.<Person>}
     */
    get list() {
        if (0 === this.#filter.length) {
            return this.#list.sort(Persons.#sortList.bind(this));
        }

        return this.#fuse.search(this.#filter).map((finding) => {
            return finding.item;
        });
    }

    /**
     * @param {Person} itemLeft
     * @param {Person} itemRight
     * @returns {number}
     */
    static #sortList(itemLeft, itemRight) {
        return itemLeft.name.localeCompare(itemRight.name);
    }

    /**
     * @param {string} filter
     * @returns {Persons}
     */
    search(filter) {
        return new Persons(
            this.total,
            this.list,
            filter,
        );
    }

    /**
     * @param {string} id
     * @returns {Person}
     */
    fetchById(id) {
        return this.#list.find((person) => {
            return person.id === id;
        });
    }
}

export default Persons;
