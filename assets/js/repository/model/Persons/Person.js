'use strict';

class Person {
    /** @var {string} */
    #id;
    /** @var {string} */
    #name;

    /**
     * @param {string} id
     * @param {string} name
     */
    constructor(id, name) {
        this.#id  = id;
        this.#name = name;
    }

    /**
     * @param raw
     * @returns {Person}
     */
    static parse(raw) {
        return new Person(
            raw.id,
            raw.name,
        );
    }

    /**
     * @returns {string}
     */
    get id() {
        return this.#id;
    }

    /**
     * @returns {string}
     */
    get name() {
        return this.#name;
    }
}

export default Person;
