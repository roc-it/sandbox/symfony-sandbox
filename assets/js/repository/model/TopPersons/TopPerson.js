'use strict';

class TopPerson {
    /** @var {string} */
    #personId;
    /** @var {string} */
    #personName;
    /** @var {number} */
    #gameCount;
    /** @var {number} */
    #victoryCount;
    /** @var {number|null} */
    #averageRank;
    /** @var {boolean} */
    #isWinner;

    /**
     * @param {string} personId
     * @param {string} personName
     * @param {number} gameCount
     * @param {number} victoryCount
     * @param {number|null} averageRank
     * @param {boolean} isWinner
     */
    constructor(
        personId,
        personName,
        gameCount,
        victoryCount,
        averageRank,
        isWinner,
    ) {
        this.#personId     = personId;
        this.#personName   = personName;
        this.#gameCount    = gameCount;
        this.#victoryCount = victoryCount;
        this.#averageRank  = averageRank;
        this.#isWinner     = isWinner;
    }

    /**
     * @param raw
     * @returns {TopPerson}
     */
    static parse(raw) {
        return new TopPerson(
            raw.personId,
            raw.personName,
            raw.gameCount,
            raw.victoryCount,
            raw.averageRank,
            raw.isWinner,
        );
    }

    /**
     * @returns {string}
     */
    get personId() {
        return this.#personId;
    }

    /**
     * @returns {string}
     */
    get personName() {
        return this.#personName;
    }

    /**
     * @returns {number}
     */
    get gameCount() {
        return this.#gameCount;
    }

    /**
     * @returns {number}
     */
    get victoryCount() {
        return this.#victoryCount;
    }

    /**
     * @returns {number|null}
     */
    get averageRank() {
        return this.#averageRank;
    }

    /**
     * @returns {boolean}
     */
    get isWinner() {
        return this.#isWinner;
    }
}

export default TopPerson;
