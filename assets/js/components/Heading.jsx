import PropTypes from 'prop-types';
import React from 'react';

export default function Heading({ children }) {
    return (
        <h2 className="text-2xl font-bold mb-2">
            {children}
        </h2>
    );
}

Heading.propTypes = {
    children: PropTypes.node,
};

Heading.defaultProps = {
    children: null,
};
