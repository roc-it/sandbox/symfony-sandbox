# Install

See [here](https://gitlab.com/roc-it/sandbox/symfony-sandbox-infra)

# Init

:warning: Make sur `functions.sh` is sourced before running those to ensure you are executing everything on the containers. Otherwise adapt the commands before running them.

```bash
$ yarn install --frozen-lockfile
$ composer install
$ dev do:mi:mi -n
$ dev h:f:l -n
$ yarn watch
```
