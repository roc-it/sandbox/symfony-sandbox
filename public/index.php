<?php

declare(strict_types=1);

use Infrastructure\Kernel;
use Symfony\Component\HttpFoundation\Request;

require_once dirname(__DIR__) . '/vendor/autoload_runtime.php';

return static function (array $context, Request $request): Kernel {
    if ($trustedProxies = $context['TRUSTED_PROXIES'] ?? false) {
        $request::setTrustedProxies(explode(',', $trustedProxies), Request::HEADER_X_FORWARDED_TRAEFIK ^ Request::HEADER_X_FORWARDED_HOST);
    }

    if ($trustedHosts = $context['TRUSTED_HOSTS'] ?? false) {
        $request::setTrustedHosts([$trustedHosts]);
    }

    $env   = $context['APP_ENV'] ?? 'dev';
    $debug = (bool) ($context['APP_DEBUG'] ?? ('prod' !== $env));

    if (isset($context['APP_SWITCH_ENV']) && 'true' === $context['APP_SWITCH_ENV']) {
        if ($request->headers->has('APP-ENV')) {
            $env = $request->headers->get('APP-ENV');
        }
        if ($request->headers->has('APP-DEBUG')) {
            $debug = 'true' === $request->headers->get('APP-DEBUG');
        }
    }

    return new Kernel($env, $debug);
};
