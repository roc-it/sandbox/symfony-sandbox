<?php

declare(strict_types=1);

namespace Application\Web\Views;

use Symfony\Component\Form\FormView;

final class AddChallenger
{
    public function __construct(
        public readonly FormView $form
    ) {
    }
}
