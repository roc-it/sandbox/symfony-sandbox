<?php

declare(strict_types=1);

namespace Application\Web\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Twig\Environment;

#[Route(
    name: 'try_vulcain',
    path: '/vulcain',
    methods: [Request::METHOD_GET]
)]
final class TryVulcain
{
    public function __construct(private readonly Environment $twig)
    {
    }

    public function __invoke(): Response
    {
        return new Response($this->twig->render('try_vulcain.html.twig'));
    }
}
