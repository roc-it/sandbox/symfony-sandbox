<?php

declare(strict_types=1);

namespace Application\Web\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Twig\Environment;

#[Route(
    name: 'homepage',
    path: '/',
    methods: [Request::METHOD_GET]
)]
final class Homepage
{
    public function __construct(private readonly Environment $twig)
    {
    }

    public function __invoke(Request $request): Response
    {
        return new Response($this->twig->render(
            'homepage/homepage_react.html.twig'
        ));
    }
}
