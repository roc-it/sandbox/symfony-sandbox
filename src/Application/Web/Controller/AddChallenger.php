<?php

declare(strict_types=1);

namespace Application\Web\Controller;

use Application\Web\Views\AddChallenger as AddChallengerViewModel;
use Domain\Tetris\Command\CreateChallengerCommand;
use Infrastructure\Form\AddChallengerType;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;
use Twig\Environment;

#[Route(
    name: 'challenger_add',
    path: '/challengers/add',
    methods: [Request::METHOD_GET, Request::METHOD_POST]
)]
final class AddChallenger
{
    public function __construct(
        private readonly FormFactoryInterface $formFactory,
        private readonly Environment $twig,
        private readonly MessageBusInterface $commandBus
    ) {
    }

    public function __invoke(Request $request): Response
    {
        $command = new CreateChallengerCommand();

        $form = $this->formFactory->create(AddChallengerType::class, $command, [
            'validation_groups' => ['form'],
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $command->id = Uuid::uuid4()->toString();
            $this->commandBus->dispatch($command);
        }

        return new Response($this->twig->render('add_challenger.html.twig', [
            'data' => new AddChallengerViewModel(
                form: $form->createView()
            ),
        ]));
    }
}
