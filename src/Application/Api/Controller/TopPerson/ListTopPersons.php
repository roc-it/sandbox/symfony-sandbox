<?php

declare(strict_types=1);

namespace Application\Api\Controller\TopPerson;

use Domain\Tetris\Query\ListTopPersonsQuery;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\HandleTrait;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\SerializerInterface;

#[Route(
    name: 'api_toppersons_list',
    path: '/top-persons',
    methods: [Request::METHOD_GET]
)]
final class ListTopPersons
{
    use HandleTrait;

    public function __construct(
        private readonly SerializerInterface $serializer,
        MessageBusInterface         $queryBus
    ) {
        $this->messageBus = $queryBus;
    }

    public function __invoke(Request $request): JsonResponse
    {
        $query = new ListTopPersonsQuery();

        if ($request->query->has('limit') === true) {
            $query->limit = $request->query->getInt('limit');
        }

        $topPersons = $this->handle($query);

        return new JsonResponse(
            $this->serializer->serialize($topPersons, JsonEncoder::FORMAT),
            Response::HTTP_OK,
            [],
            true
        );
    }
}
