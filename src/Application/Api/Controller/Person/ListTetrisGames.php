<?php

declare(strict_types=1);

namespace Application\Api\Controller\Person;

use Domain\Tetris\Query\ListTetrisGameQuery;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\HandleTrait;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\SerializerInterface;

#[Route(
    name: 'api_list_persons_tetris_games',
    path: '/persons/{id}/tetris-games',
    methods: [Request::METHOD_GET]
)]
final class ListTetrisGames
{
    use HandleTrait;

    public function __construct(
        private readonly SerializerInterface $serializer,
        MessageBusInterface         $queryBus
    ) {
        $this->messageBus = $queryBus;
    }

    public function __invoke(Request $request, string $id): JsonResponse
    {
        $query           = new ListTetrisGameQuery();
        $query->personId = $id;

        $tetrisGames = $this->handle($query);

        return new JsonResponse(
            $this->serializer->serialize($tetrisGames, JsonEncoder::FORMAT),
            Response::HTTP_OK,
            [],
            true
        );
    }
}
