<?php

declare(strict_types=1);

namespace Application\Api\Controller\TetrisGame;

use Domain\Tetris\Command\CreateTetrisGameCommand;
use Domain\Tetris\Query\FetchTetrisGameQuery;
use Ramsey\Uuid\Uuid;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\HandleTrait;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\AbstractObjectNormalizer;
use Symfony\Component\Serializer\SerializerInterface;

#[Route(
    name: 'api_create_tetris_games',
    path: '/tetris-games',
    methods: [Request::METHOD_POST]
)]
final class CreateTetrisGame
{
    use HandleTrait;

    public function __construct(
        private readonly SerializerInterface $serializer,
        private readonly MessageBusInterface $commandBus,
        MessageBusInterface         $queryBus
    ) {
        $this->messageBus = $queryBus;
    }

    public function __invoke(Request $request): JsonResponse
    {
        $command     = new CreateTetrisGameCommand();
        $command->id = Uuid::uuid4()->toString();

        /** @var CreateTetrisGameCommand $command */
        $command = $this->serializer->deserialize(
            $request->getContent(),
            CreateTetrisGameCommand::class,
            $request->getRequestFormat(JsonEncoder::FORMAT),
            [
                AbstractObjectNormalizer::OBJECT_TO_POPULATE       => $command,
                AbstractObjectNormalizer::DISABLE_TYPE_ENFORCEMENT => true,
            ]
        );

        $this->commandBus->dispatch($command);

        $query               = new FetchTetrisGameQuery();
        $query->tetrisGameId = $command->id;

        $tetrisGame = $this->handle($query);

        return new JsonResponse(
            $this->serializer->serialize($tetrisGame, JsonEncoder::FORMAT),
            Response::HTTP_CREATED,
            [],
            true
        );
    }
}
