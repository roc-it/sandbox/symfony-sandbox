<?php

declare(strict_types=1);

namespace Application\Api\Controller\TetrisGame;

use Domain\Tetris\Query\FetchTetrisGameQuery;
use Domain\Tetris\ReadModel\TetrisGame;
use Fig\Link\GenericLinkProvider;
use Fig\Link\Link;
use Psr\Link\EvolvableLinkProviderInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\HandleTrait;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\SerializerInterface;

#[Route(
    name: 'api_fetch_tetris_games',
    path: '/tetris-games/{tetrisGameId}',
    methods: [Request::METHOD_GET]
)]
final class FetchTetrisGame
{
    use HandleTrait;

    public function __construct(
        private readonly SerializerInterface $serializer,
        private readonly RouterInterface     $router,
        MessageBusInterface         $queryBus
    ) {
        $this->messageBus = $queryBus;
    }

    public function __invoke(Request $request, string $tetrisGameId): JsonResponse
    {
        $query               = new FetchTetrisGameQuery();
        $query->tetrisGameId = $tetrisGameId;

        /** @var TetrisGame $tetrisGame */
        $tetrisGame = $this->handle($query);

        /** @var EvolvableLinkProviderInterface $linkProvider */
        $linkProvider = $request->attributes->get('_links', new GenericLinkProvider());
        $request->attributes->set('_links', $linkProvider->withLink(
            new Link('preload', $this->router->generate('api_tetris_game_challengers_list', ['tetrisGameId' => $tetrisGameId]))
        ));

        return new JsonResponse(
            $this->serializer->serialize($tetrisGame, JsonEncoder::FORMAT),
            Response::HTTP_OK,
            [],
            true
        );
    }
}
