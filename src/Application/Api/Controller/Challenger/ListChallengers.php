<?php

declare(strict_types=1);

namespace Application\Api\Controller\Challenger;

use Domain\Tetris\Query\ListTetrisGameChallengersQuery;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\HandleTrait;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\SerializerInterface;

#[Route(
    name: 'api_tetris_game_challengers_list',
    path: '/tetris-games/{tetrisGameId}/challengers',
    methods: [Request::METHOD_GET]
)]
final class ListChallengers
{
    use HandleTrait;

    public function __construct(
        private readonly SerializerInterface $serializer,
        MessageBusInterface         $queryBus
    ) {
        $this->messageBus = $queryBus;
    }

    public function __invoke(string $tetrisGameId): JsonResponse
    {
        $query               = new ListTetrisGameChallengersQuery();
        $query->tetrisGameId = $tetrisGameId;

        $tetrisGames = $this->handle($query);

        return new JsonResponse(
            $this->serializer->serialize($tetrisGames, JsonEncoder::FORMAT),
            Response::HTTP_OK,
            [],
            true
        );
    }
}
