<?php

declare(strict_types=1);

namespace Application\Api\Controller\Challenger;

use Domain\Tetris\Command\CreateChallengersCommand;
use Domain\Tetris\Command\CreateChallengersCommand\Challenger;
use Domain\Tetris\Query\ListTetrisGameChallengersQuery;
use Ramsey\Uuid\Uuid;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\HandleTrait;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\AbstractObjectNormalizer;
use Symfony\Component\Serializer\SerializerInterface;
use function array_walk;

#[Route(
    name: 'api_tetris_game_challengers_add',
    path: '/tetris-games/{tetrisGameId}/challengers',
    methods: [Request::METHOD_POST]
)]
final class AddChallengers
{
    use HandleTrait;

    public function __construct(
        private readonly SerializerInterface $serializer,
        private readonly MessageBusInterface $commandBus,
        MessageBusInterface                  $queryBus
    ) {
        $this->messageBus = $queryBus;
    }

    public function __invoke(Request $request, string $tetrisGameId): JsonResponse
    {
        $command               = new CreateChallengersCommand();
        $command->tetrisGameId = $tetrisGameId;
        $command->challengers  = $this->serializer->deserialize(
            $request->getContent(),
            CreateChallengersCommand\Challengers::class,
            $request->getRequestFormat(JsonEncoder::FORMAT),
            [
                AbstractObjectNormalizer::DISABLE_TYPE_ENFORCEMENT => true,
            ]
        );

        array_walk(
            $command->challengers->list,
            static function (Challenger $challenger): void {
                $challenger->id = $challenger->id ?: Uuid::uuid4()->toString();
            }
        );

        $this->commandBus->dispatch($command);

        $query               = new ListTetrisGameChallengersQuery();
        $query->tetrisGameId = $command->tetrisGameId;

        $challengers = $this->handle($query);

        return new JsonResponse(
            $this->serializer->serialize($challengers, JsonEncoder::FORMAT),
            Response::HTTP_CREATED,
            [],
            true
        );
    }
}
