<?php

declare(strict_types=1);

namespace Application\Cli;

use ApiPlatform\Core\Bridge\Symfony\Validator\Exception\ValidationException;
use Domain\Tetris\Command\CreatePersonCommand;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use function count;

final class AddPerson extends Command
{
    protected static $defaultName = 'app:person:add';

    protected static $defaultDescription = 'Create a new person';

    public function __construct(
        private readonly MessageBusInterface $messageBus,
        private readonly ValidatorInterface $validator
    ) {
        parent::__construct(null);
    }

    /**
     * {@inheritDoc}
     */
    protected function configure(): void
    {
        $this
            ->addArgument('name', InputArgument::REQUIRED, 'Name of the person.');
    }

    protected function interact(InputInterface $input, OutputInterface $output): void
    {
        $io = new SymfonyStyle($input, $output);

        if ($input->getArgument('name') === null) {
            $name = $io->ask('Name of the challenger');

            $input->setArgument('name', $name ?? '');
        }
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $command = new CreatePersonCommand();

        $command->id   = Uuid::uuid4()->toString();
        $command->name = $input->getArgument('name');

        $violations = $this->validator->validate($command);
        if (0 !== count($violations)) {
            throw new ValidationException($violations);
        }

        $this->messageBus->dispatch($command);

        $io = new SymfonyStyle($input, $output);
        $io->success("User with name '{$command->name}' created.");

        return 0;
    }
}
