<?php

declare(strict_types=1);

namespace Infrastructure\Tetris\Query;

use Doctrine\Persistence\ManagerRegistry;
use Domain\Tetris\Query\ListTetrisGameChallengersQuery;
use Domain\Tetris\ReadModel\Challenger;
use Infrastructure\Entity\Challenger as ChallengerEntity;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use function array_map;

final class ListTetrisGameChallengersQueryHandler implements MessageHandlerInterface
{
    public function __construct(private readonly ManagerRegistry $registry)
    {
    }

    /**
     * @return \Domain\Tetris\ReadModel\Challenger[]
     */
    public function __invoke(ListTetrisGameChallengersQuery $listTetrisGameChallengersQuery): array
    {
        $em         = $this->registry->getManagerForClass(ChallengerEntity::class);
        $repository = $em->getRepository(ChallengerEntity::class);

        $qb = $repository->createQueryBuilder('challenger');

        $qb
            ->join('challenger.person', 'person')
            ->where($qb->expr()->eq('challenger.tetrisGame', ':tetrisGame'))
            ->setParameter('tetrisGame', $listTetrisGameChallengersQuery->tetrisGameId)
            ->orderBy('challenger.rank', 'ASC')
        ;

        $challengerEntities = $qb->getQuery()->getResult();

        return array_map(static function (ChallengerEntity $challengerEntity): Challenger {
            $challenger           = new Challenger();
            $challenger->personId = $challengerEntity->person->id;
            $challenger->rank     = $challengerEntity->rank;
            $challenger->name     = $challengerEntity->person->name;

            return $challenger;
        }, $challengerEntities);
    }
}
