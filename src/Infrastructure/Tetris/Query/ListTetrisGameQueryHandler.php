<?php

declare(strict_types=1);

namespace Infrastructure\Tetris\Query;

use Doctrine\Persistence\ManagerRegistry;
use Domain\Tetris\Query\ListTetrisGameQuery;
use Domain\Tetris\ReadModel\TetrisGame;
use Infrastructure\Entity\TetrisGame as TetrisGameEntity;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use function array_map;

final class ListTetrisGameQueryHandler implements MessageHandlerInterface
{
    public function __construct(private readonly ManagerRegistry $registry)
    {
    }

    /**
     * @return TetrisGame[]
     */
    public function __invoke(ListTetrisGameQuery $listTetrisGameQuery): array
    {
        $em = $this->registry->getManagerForClass(TetrisGameEntity::class);

        $tetrisGameRepository = $em->getRepository(TetrisGameEntity::class);

        $qb = $tetrisGameRepository->createQueryBuilder('tetris_game');

        $qb
            ->orderBy('tetris_game.date', 'DESC');

        if (null !== $listTetrisGameQuery->personId) {
            $qb
                ->innerJoin('tetris_game.challengers', 'challengers')
                ->innerJoin('challengers.person', 'person')
                ->andWhere($qb->expr()->eq('person.id', ':personId'))
                ->setParameter('personId', $listTetrisGameQuery->personId)
            ;
        }

        $tetrisGameEntities = $qb->getQuery()->getResult();

        return array_map(
            static function (TetrisGameEntity $tetrisGameEntity): TetrisGame {
                $tetrisGame       = new TetrisGame();
                $tetrisGame->id   = $tetrisGameEntity->id;
                $tetrisGame->date = $tetrisGameEntity->date->format('d/m/Y');

                return $tetrisGame;
            },
            $tetrisGameEntities
        );
    }
}
