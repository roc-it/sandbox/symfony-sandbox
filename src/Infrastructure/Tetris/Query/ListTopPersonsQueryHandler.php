<?php

declare(strict_types=1);

namespace Infrastructure\Tetris\Query;

use Doctrine\ORM\AbstractQuery;
use Domain\Tetris\Query\ListTopPersonsQuery;
use Domain\Tetris\ReadModel\TopPerson;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Query\ResultSetMapping;
use Doctrine\Persistence\ManagerRegistry;
use function array_keys;
use function array_map;
use function round;

final class ListTopPersonsQueryHandler
{
    public function __construct(private readonly ManagerRegistry $registry)
    {
    }

    public function __invoke(ListTopPersonsQuery $query): array
    {
        /** @var EntityManager $manager */
        $manager = $this->registry->getManager('default');

        $sql = <<<'PGSQL'
        SELECT 
            person.person_id AS "person_id", 
            person.name AS "person_name", 
            COUNT(challenger.challenger_id) AS "game_count", 
            SUM(CASE WHEN challenger.rank = 1 THEN 1 ELSE 0 END) AS "victory_count", 
            AVG(challenger.rank)::NUMERIC(10,2) AS "average_rank" 
        FROM 
            person person 
                LEFT JOIN challenger challenger ON person.person_id = challenger.person_id 
        GROUP BY person.person_id 
        ORDER BY average_rank ASC, victory_count DESC
        LIMIT :limit;
        PGSQL;

        $rsm = new ResultSetMapping();
        $rsm->addScalarResult('person_id', 'person_id', 'string');
        $rsm->addScalarResult('person_name', 'person_name', 'string');
        $rsm->addScalarResult('game_count', 'game_count', 'integer');
        $rsm->addScalarResult('victory_count', 'victory_count', 'integer');
        $rsm->addScalarResult('average_rank', 'average_rank', 'float');

        $sqlQuery = $manager->createNativeQuery($sql, $rsm);

        $topPersonArrays = $sqlQuery->execute(
            [
                'limit' => $query->limit
            ],
            AbstractQuery::HYDRATE_OBJECT
        );

        return array_map(
            static function (array $topPersonArray, $index): TopPerson {
                $topPerson               = new TopPerson();
                $topPerson->personId     = $topPersonArray['person_id'];
                $topPerson->personName   = $topPersonArray['person_name'];
                $topPerson->gameCount    = $topPersonArray['game_count'];
                $topPerson->victoryCount = $topPersonArray['victory_count'];
                $topPerson->isWinner     = 0 === $index;

                $averageRank            = $topPersonArray['average_rank'];
                $topPerson->averageRank = (null !== $averageRank) ? (int) round($averageRank) : null;

                return $topPerson;
            },
            $topPersonArrays,
            array_keys($topPersonArrays),
        );
    }
}
