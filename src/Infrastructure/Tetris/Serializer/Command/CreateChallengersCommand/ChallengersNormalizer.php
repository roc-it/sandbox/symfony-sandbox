<?php

declare(strict_types=1);

namespace Infrastructure\Tetris\Serializer\Command\CreateChallengersCommand;

use Domain\Tetris\Command\CreateChallengersCommand\Challenger;
use Domain\Tetris\Command\CreateChallengersCommand\Challengers;
use Symfony\Component\Serializer\Normalizer\DenormalizerAwareInterface;
use Symfony\Component\Serializer\Normalizer\DenormalizerAwareTrait;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use function sprintf;

final class ChallengersNormalizer implements DenormalizerInterface, DenormalizerAwareInterface
{
    use DenormalizerAwareTrait;

    public function denormalize($data, string $type, string $format = null, array $context = []): Challengers
    {
        return new Challengers(
            $this->denormalizer->denormalize($data, sprintf('%s[]', Challenger::class), $format, $context)
        );
    }

    public function supportsDenormalization($data, string $type, string $format = null): bool
    {
        return $type === Challengers::class;
    }
}
