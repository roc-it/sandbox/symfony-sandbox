<?php

declare(strict_types=1);

namespace Infrastructure\Tetris\Command;

use Doctrine\Persistence\ManagerRegistry;
use Domain\Tetris\Command\CreateChallengersCommand;
use Domain\Tetris\Command\CreateChallengersCommand\Challenger;
use Infrastructure\Entity\Challenger as ChallengerEntity;
use Infrastructure\Entity\Person;
use Infrastructure\Entity\TetrisGame;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use function array_reduce;

final class CreateChallengersCommandHandler implements MessageHandlerInterface
{
    public function __construct(private readonly ManagerRegistry $registry)
    {
    }

    public function __invoke(CreateChallengersCommand $createChallengersCommand): void
    {
        $em = $this->registry->getManagerForClass(ChallengerEntity::class);

        $tetrisGame = $em->getReference(TetrisGame::class, $createChallengersCommand->tetrisGameId);

        $challengers = array_reduce(
            $createChallengersCommand->challengers->list,
            static function (array $challengers, Challenger $challenger) use ($em, $tetrisGame): array {
                $challengerEntity             = new ChallengerEntity();
                $challengerEntity->person     = $em->getReference(Person::class, $challenger->personId);
                $challengerEntity->tetrisGame = $tetrisGame;
                $challengerEntity->id         = $challenger->id;
                $challengerEntity->rank       = $challenger->rank;

                $em->persist($challengerEntity);
                $challengers[] = $challengerEntity;

                return $challengers;
            },
            []
        );
    }
}
