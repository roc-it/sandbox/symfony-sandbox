<?php

declare(strict_types=1);

namespace Infrastructure\Tetris\Command;

use Doctrine\Persistence\ManagerRegistry;
use Domain\Tetris\Command\CreatePersonCommand;
use Infrastructure\Entity\Person as PersonEntity;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class CreatePersonCommandHandler implements MessageHandlerInterface
{
    public function __construct(private readonly ManagerRegistry $registry)
    {
    }

    public function __invoke(CreatePersonCommand $createPersonCommand): void
    {
        $em = $this->registry->getManagerForClass(PersonEntity::class);

        $personEntity = new PersonEntity();
        $personEntity->id = $createPersonCommand->id;
        $personEntity->name = $createPersonCommand->name;

        $em->persist($personEntity);
    }
}
