<?php

declare(strict_types=1);

namespace Infrastructure\Tetris\Command;

use Doctrine\Persistence\ManagerRegistry;
use Domain\Tetris\Command\CreateChallengerCommand;
use Infrastructure\Entity\Challenger;
use Infrastructure\Entity\Person;
use Infrastructure\Entity\TetrisGame;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class CreateChallengerCommandHandler implements MessageHandlerInterface
{
    public function __construct(private readonly ManagerRegistry $registry)
    {
    }

    public function __invoke(CreateChallengerCommand $createChallengerCommand): void
    {
        $em = $this->registry->getManagerForClass(Challenger::class);

        $challenger             = new Challenger();
        $challenger->person     = $em->getReference(Person::class, $createChallengerCommand->personId);
        $challenger->tetrisGame = $em->getReference(TetrisGame::class, $createChallengerCommand->tetrisGameId);
        $challenger->id         = $createChallengerCommand->id;
        $challenger->rank       = $createChallengerCommand->rank;

        $em->persist($challenger);
    }
}
