<?php

declare(strict_types=1);

namespace Infrastructure\Tetris\Command;

use Domain\Tetris\Command\CreateTetrisGameCommand;
use Doctrine\Persistence\ManagerRegistry;
use Infrastructure\Entity\TetrisGame as TetrisGameEntity;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class CreateTetrisGameCommandHandler implements MessageHandlerInterface
{
    public function __construct(private readonly ManagerRegistry $registry)
    {
    }

    public function __invoke(CreateTetrisGameCommand $createTetrisGameCommand): void
    {
        $em = $this->registry->getManagerForClass(TetrisGameEntity::class);

        $tetrisGameEntity = new TetrisGameEntity();
        $tetrisGameEntity->id = $createTetrisGameCommand->id;
        $tetrisGameEntity->date = $createTetrisGameCommand->date;

        $em->persist($tetrisGameEntity);
    }
}
