<?php

declare(strict_types=1);

namespace Infrastructure\Serializer;

use Domain\Serializer\SerializerInterface as DomainSerializerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\AbstractObjectNormalizer;
use Symfony\Component\Serializer\SerializerInterface;
use function array_merge;

final class Serializer implements DomainSerializerInterface
{
    private const DEFAULT_FORMAT = JsonEncoder::FORMAT;

    public function __construct(
        private readonly SerializerInterface $serializer,
        private readonly RequestStack $requestStack,
    ) {
    }

    /**
     * {@inheritdoc}
     */
    public function deserialize(mixed $data, string $type, array $context = []): mixed
    {
        $request = $this->requestStack->getCurrentRequest();

        $format = self::DEFAULT_FORMAT;
        if (null !== $request) {
            $format = $request->getRequestFormat(self::DEFAULT_FORMAT) ?? self::DEFAULT_FORMAT;
        }

        /** @var T $result */
        $result = $this->serializer->deserialize(
            $data,
            $type,
            $format,
            array_merge([
                AbstractObjectNormalizer::DISABLE_TYPE_ENFORCEMENT => true,
            ], $context)
        );

        return $result;
    }
}
