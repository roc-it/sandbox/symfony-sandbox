<?php

declare(strict_types=1);

namespace Infrastructure\Form\Dto;

use Infrastructure\Entity\TetrisGame;

final class AddChallengerDto
{
    public TetrisGame $tetrisGame;
}
