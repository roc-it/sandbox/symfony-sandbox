<?php

declare(strict_types=1);

namespace Infrastructure\Form\Choices;

use Doctrine\Persistence\ManagerRegistry;
use Generator;
use Infrastructure\Entity\TetrisGame;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\ChoiceList\Loader\CallbackChoiceLoader;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\OptionsResolver\OptionsResolver;

final class TetrisGameChoiceType extends AbstractType
{
    public function __construct(private readonly ManagerRegistry $registry)
    {
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'choices'                   => null,
            'choice_loader'             => new CallbackChoiceLoader(function (): Generator {
                $em         = $this->registry->getManagerForClass(TetrisGame::class);
                $repository = $em->getRepository(TetrisGame::class);

                $qb = $repository->createQueryBuilder('tetris_game');

                $qb
                    ->orderBy('tetris_game.date', 'DESC');

                /** @var TetrisGame[] $tetrisGames */
                $tetrisGames = $qb->getQuery()->getResult();

                foreach ($tetrisGames as $tetrisGame) {
                    yield $tetrisGame->date->format('d/m/Y') => $tetrisGame->id;
                }
            }),
            'choice_translation_domain' => false,
        ]);
    }

    public function getParent(): string
    {
        return ChoiceType::class;
    }
}
