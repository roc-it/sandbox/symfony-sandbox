<?php

declare(strict_types=1);

namespace Infrastructure\Form\Choices;

use Doctrine\Persistence\ManagerRegistry;
use Generator;
use Infrastructure\Entity\Person;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\ChoiceList\Loader\CallbackChoiceLoader;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\OptionsResolver\OptionsResolver;

final class PersonChoiceType extends AbstractType
{
    public function __construct(private readonly ManagerRegistry $registry)
    {
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'choices'                   => null,
            'choice_loader'             => new CallbackChoiceLoader(function (): Generator {
                $em         = $this->registry->getManagerForClass(Person::class);
                $repository = $em->getRepository(Person::class);

                $qb = $repository->createQueryBuilder('person');

                $qb
                    ->orderBy('person.name', 'ASC');

                /** @var Person[] $persons */
                $persons = $qb->getQuery()->getResult();

                foreach ($persons as $person) {
                    yield $person->name => $person->id;
                }
            }),
            'choice_translation_domain' => false,
        ]);
    }

    public function getParent(): string
    {
        return ChoiceType::class;
    }
}
