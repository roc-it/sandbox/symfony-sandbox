<?php

declare(strict_types=1);

namespace Infrastructure\Form;

use Domain\Tetris\Command\CreateChallengerCommand;
use Infrastructure\Form\Choices\PersonChoiceType;
use Infrastructure\Form\Choices\TetrisGameChoiceType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

final class AddChallengerType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('tetrisGameId', TetrisGameChoiceType::class, [
                'multiple'           => false,
                'expanded'           => false,
                'label'              => 'form.tetris.game',
                'translation_domain' => 'messages',
            ])
            ->add('personId', PersonChoiceType::class, [
                'multiple' => false,
                'expanded' => false,
                'label'    => 'form.person.name',
            ])
            ->add('rank', IntegerType::class, [
                'label' => 'form.challenger.rank'
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver
            ->setDefaults([
                'data_class'        => CreateChallengerCommand::class,
                'validation_groups' => ['form'],
            ]);
    }
}
