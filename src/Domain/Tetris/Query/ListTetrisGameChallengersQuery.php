<?php

declare(strict_types=1);

namespace Domain\Tetris\Query;

use Symfony\Component\Validator\Constraints as Assert;

final class ListTetrisGameChallengersQuery
{
    #[Assert\NotNull]
    #[Assert\NotBlank(allowNull: true)]
    public string $tetrisGameId;
}
