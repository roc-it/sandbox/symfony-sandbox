<?php

declare(strict_types=1);

namespace Domain\Tetris\Query;

final class ListTetrisGameQuery
{
    public string|null $personId = null;
}
