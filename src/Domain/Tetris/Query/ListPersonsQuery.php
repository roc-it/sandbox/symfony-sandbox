<?php

declare(strict_types=1);

namespace Domain\Tetris\Query;

final class ListPersonsQuery
{
    public int $page = 1;

    public int $max = 10;
}
