<?php

declare(strict_types=1);

namespace Domain\Tetris\Query;

use Symfony\Component\Validator\Constraints as Assert;

final class FetchTetrisGameQuery
{
    #[Assert\NotNull]
    #[Assert\NotBlank(allowNull: true)]
    public string $tetrisGameId;
}
