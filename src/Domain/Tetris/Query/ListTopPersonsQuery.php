<?php

declare(strict_types=1);

namespace Domain\Tetris\Query;

final class ListTopPersonsQuery
{
    public int $limit = 30;
}
