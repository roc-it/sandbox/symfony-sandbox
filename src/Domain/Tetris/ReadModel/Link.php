<?php

declare(strict_types=1);

namespace Domain\Tetris\ReadModel;

final class Link
{
    public string $url;
}
