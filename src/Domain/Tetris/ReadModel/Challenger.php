<?php

declare(strict_types=1);

namespace Domain\Tetris\ReadModel;

final class Challenger
{
    public string $personId;

    public string $name;

    public int $rank;
}
