<?php

declare(strict_types=1);

namespace Domain\Tetris\ReadModel;

final class Person
{
    public string $id;

    public string $name;

    /**
     * @var Link[]
     */
    public array $_links = [];
}
