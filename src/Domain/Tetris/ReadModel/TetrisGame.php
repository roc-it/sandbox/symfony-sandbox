<?php

declare(strict_types=1);

namespace Domain\Tetris\ReadModel;

final class TetrisGame
{
    public string $id;

    public string $date;
}
