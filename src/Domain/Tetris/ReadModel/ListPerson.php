<?php

declare(strict_types=1);

namespace Domain\Tetris\ReadModel;

final class ListPerson
{
    public int $page;

    public int $count;

    /**
     * @var Person[]
     */
    public array $persons;

    /**
     * @var Link[]
     */
    public array $_links = [];
}
