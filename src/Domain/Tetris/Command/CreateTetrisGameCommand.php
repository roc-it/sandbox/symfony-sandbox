<?php

declare(strict_types=1);

namespace Domain\Tetris\Command;

use DateTimeImmutable;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotNull;
use Symfony\Component\Validator\Constraints\Type;

final class CreateTetrisGameCommand
{
    #[NotNull()]
    #[NotBlank(allowNull: true)]
    public string $id;

    #[Type(DateTimeImmutable::class)]
    public DateTimeImmutable $date;
}
