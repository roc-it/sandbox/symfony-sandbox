<?php

declare(strict_types=1);

namespace Domain\Tetris\Command\CreateChallengersCommand;

use ArrayIterator;
use IteratorAggregate;
use Symfony\Component\Validator\Constraints\Count;
use Traversable;

/**
 * @phpstan-implements IteratorAggregate<int, Challenger>
 */
final class Challengers implements IteratorAggregate
{
    /**
     * @var array<int, Challenger>
     */
    #[Count(min: 1)]
    public array $list;

    /**
     * @param array<int, Challenger> $list
     */
    public function __construct(array $list)
    {
        $this->list = $list;
    }

    /**
     * @return Traversable<int, Challenger>
     */
    public function getIterator(): Traversable
    {
        return new ArrayIterator($this->list);
    }
}
