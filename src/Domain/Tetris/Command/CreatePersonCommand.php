<?php

declare(strict_types=1);

namespace Domain\Tetris\Command;

use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotNull;

final class CreatePersonCommand
{
    #[NotNull()]
    #[Length(min: 1)]
    public string $id;

    #[NotNull()]
    #[Length(min: 1)]
    public string $name;
}
