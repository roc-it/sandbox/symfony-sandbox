<?php

declare(strict_types=1);

namespace Domain\Tetris\Command;

use Domain\Tetris\Command\CreateChallengersCommand\Challenger;
use Domain\Tetris\Command\CreateChallengersCommand\Challengers;
use Symfony\Component\Validator\Constraints\Count;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotNull;
use Symfony\Component\Validator\Constraints\Type;
use Symfony\Component\Validator\Constraints\Valid;

final class CreateChallengersCommand
{
    #[NotNull(groups: ['Default', 'form'])]
    #[NotBlank(allowNull: true, groups: ['Default', 'form'])]
    #[Type('string', groups: ['Default', 'form'])]
    public string $tetrisGameId;

    #[Valid(groups: ['Default', 'form'])]
    public Challengers $challengers;
}
