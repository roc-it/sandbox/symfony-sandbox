<?php

declare(strict_types=1);

namespace Domain\Tetris\Command;

use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotNull;
use Symfony\Component\Validator\Constraints\Range;
use Symfony\Component\Validator\Constraints\Type;

final class CreateChallengerCommand
{
    #[NotNull()]
    #[NotBlank(allowNull: true)]
    #[Type('string')]
    public ?string $id = null;

    #[NotNull(groups: ['Default', 'form'])]
    #[NotBlank(allowNull: true, groups: ['Default', 'form'])]
    #[Type('string', groups: ['Default', 'form'])]
    public ?string $personId = null;

    #[NotNull(groups: ['Default', 'form'])]
    #[NotBlank(allowNull: true, groups: ['Default', 'form'])]
    #[Type('string', groups: ['Default', 'form'])]
    public ?string $tetrisGameId = null;

    #[NotNull(groups: ['Default', 'form'])]
    #[NotBlank(allowNull: true, groups: ['Default', 'form'])]
    #[Type('int', groups: ['Default', 'form'])]
    #[Range(
        min: 1,
        groups: ['Default', 'form']
    )]
    public ?int $rank = null;
}
