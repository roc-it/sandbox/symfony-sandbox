<?php

declare(strict_types=1);

namespace Domain\Serializer;

interface SerializerInterface
{
    /**
     * @template T
     *
     * @param class-string<T>      $type
     * @param array<string, mixed> $context
     *
     * @return T
     */
    public function deserialize(mixed $data, string $type, array $context = []): mixed;
}
