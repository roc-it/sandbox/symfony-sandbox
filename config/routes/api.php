<?php

declare(strict_types=1);

use Symfony\Component\Routing\Loader\Configurator\RoutingConfigurator;

return static function (RoutingConfigurator $routes): void {
    $routes->import('@ApiPlatformBundle/Resources/config/routing/docs.xml');

    $routes->import('../../src/Application/Api/Controller/', 'annotation')
           ->prefix('/api', false)
           ->namePrefix('api_');
};
