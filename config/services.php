<?php

declare(strict_types=1);

namespace Symfony\Component\DependencyInjection\Loader\Configurator;

return static function (ContainerConfigurator $configurator): void {
    $services = $configurator
        ->services()
        ->defaults()
        ->autowire()
        ->autoconfigure();

    $services->load('Application\\Api\\Controller\\', '../src/Application/Api/Controller')
             ->tag('controller.service_arguments');

    $services->load('Application\\Cli\\', '../src/Application/Cli');

    $services->load('Application\\Web\\Controller\\', '../src/Application/Web/Controller')
             ->tag('controller.service_arguments');

    $services->load('Infrastructure\\Exception\\', '../src/Infrastructure/Exception');

    $services->load('Infrastructure\\Form\\', '../src/Infrastructure/Form')
             ->exclude(['../src/Infrastructure/Form/Dto']);

    $services->load('Infrastructure\\Tetris\\Command\\', '../src/Infrastructure/Tetris/Command/**/*Handler.php')
             ->autoconfigure(false)
             ->tag('messenger.message_handler', ['bus' => 'command.bus']);

    $services->load('Infrastructure\\Tetris\\Query\\', '../src/Infrastructure/Tetris/Query/**/*Handler.php')
             ->autoconfigure(false)
             ->tag('messenger.message_handler', ['bus' => 'query.bus']);

    $services->load('Infrastructure\\Tetris\\Serializer\\', '../src/Infrastructure/Tetris/Serializer/**/*.php');
};
