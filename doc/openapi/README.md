# Règles appliquées à date

* Split des fichiers models
* Un model openapi pour une ressource REST (vs un model openapi par Dto PHP)
* indent de 4 espaces
* utilisation stricte des quotes

# A tester a l'avenir

* split par paths plutôt que split par model
* utlisation de stoplight.io (IDE pour openapi)
* Un model openapi par Dto PHP
* génération systématique via la CI du openapi.json
    * exposer le fichier dans la MR pour permettre une review simplifiée avec editor.swagger.io
