<?php

declare(strict_types=1);

namespace App\Tests\functional;

use cebe\openapi\Reader;
use cebe\openapi\ReferenceContext;
use cebe\openapi\spec\Operation;
use LogicException;
use function json_decode;
use function json_encode;
use function strtolower;
use const JSON_THROW_ON_ERROR;

final class SchemaFactory
{
    public static function createFromResponse(string $path, string $method, int $statusCode): object
    {
        $openapi = Reader::readFromYamlFile(
            realpath(__DIR__ . '/../../doc/openapi/reference/Symfony-Sandbox.v1.yaml'),
            resolveReferences: ReferenceContext::RESOLVE_MODE_ALL
        );

        if ($openapi->paths->hasPath($path) === false) {
            throw new LogicException("[{$path}] Not found in openapi specs.");
        }

        $pathLevel = $openapi->paths->getPath($path);

        $method = strtolower($method);

        if (null === $pathLevel->{$method}) {
            throw new LogicException("[{$path}][{$method}] Not found in openapi specs.");
        }

        /** @var Operation $methodLevel */
        $methodLevel = $pathLevel->{$method};

        if (null === $methodLevel->responses) {
            throw new LogicException("[{$path}][{$method}] No responses defined in openapi specs.");
        }

        if (false === $methodLevel->responses->hasResponse((string) $statusCode)) {
            throw new LogicException("[{$path}][{$method}][{$statusCode}] Not found in openapi specs.");
        }

        $response = $methodLevel->responses->getResponse((string) $statusCode)?->content['application/json'] ?? null;

        if (null === $response) {
            throw new LogicException("[{$path}][{$method}][{$statusCode}]['application/json'] No content was provided for Content-Type: application/json in openapi specs.");
        }

        $schema = $response->schema;

        if (null === $schema) {
            throw new LogicException("[{$path}][{$method}][{$statusCode}]['application/json'] No schema was provided for Content-Type: application/json in openapi specs.");
        }

        return json_decode(json_encode($schema->getSerializableData(), JSON_THROW_ON_ERROR, 512), false, 512, JSON_THROW_ON_ERROR);
    }
}
