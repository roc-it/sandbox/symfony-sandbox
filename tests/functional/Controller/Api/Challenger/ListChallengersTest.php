<?php

declare(strict_types=1);

namespace App\Tests\functional\Controller\Api\Challenger;

use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\ApiTestCase;
use App\Tests\functional\SchemaFactory;
use Generator;
use Hautelook\AliceBundle\PhpUnit\RefreshDatabaseTrait;

/**
 * @coversDefaultClass \Application\Api\Controller\Challenger\ListChallengers
 * @covers ::__construct
 */
class ListChallengersTest extends ApiTestCase
{
    use RefreshDatabaseTrait;

    public function tetrisGames(): Generator
    {
        yield 'empty' => [
            'tetris_game_empty',
            0,
        ];

        yield '1 Year ago' => [
            'tetris_game_1_year_ago',
            3,
        ];

        yield '5 days ago' => [
            'tetris_game_5_days_ago',
            5,
        ];

        yield 'today' => [
            'tetris_game_today',
            5,
        ];
    }

    /**
     * @test
     * @dataProvider tetrisGames
     * @covers ::__invoke
     */
    public function retrieveListOfChallengers(string $tetrisGameId, int $expectedCountOfChallengers): void
    {
        $client = static::createClient();

        $response = $client->request('GET', "/api/tetris-games/{$tetrisGameId}/challengers");
        $schema   = SchemaFactory::createFromResponse(
            '/api/tetris-games/{tetrisGameId}/challengers',
            'GET',
            $response->getStatusCode()
        );

        self::assertResponseIsSuccessful();
        self::assertResponseHeaderSame('content-type', 'application/json');
        self::assertMatchesJsonSchema($schema);
        self::assertCount($expectedCountOfChallengers, $response->toArray(false));
    }
}
