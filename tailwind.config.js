module.exports = {
    content: [
        './templates/**/*.html.twig',
        './assets/js/**/*.{js,jsx}',
    ],
    theme: {
        extend: {},
    },
    plugins: [
        require('@tailwindcss/forms'),
    ],
}
