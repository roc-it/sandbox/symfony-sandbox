<?php

declare(strict_types=1);

use Rector\Core\Configuration\Option;
use Rector\Doctrine\Rector\Class_\RemoveRedundantDefaultClassAnnotationValuesRector;
use Rector\Doctrine\Rector\Property\RemoveRedundantDefaultPropertyAnnotationValuesRector;
use Rector\Doctrine\Set\DoctrineSetList;
use Rector\DowngradePhp70\Rector\GroupUse\SplitGroupedUseImportsRector;
use Rector\Php74\Rector\Closure\ClosureToArrowFunctionRector;
use Rector\Php74\Rector\Property\RestoreDefaultNullToNullableTypePropertyRector;
use Rector\Php81\Rector\ClassConst\FinalizePublicClassConstantRector;
use Rector\PostRector\Rector\NameImportingPostRector;
use Rector\PostRector\Rector\UseAddingPostRector;
use Rector\Privatization\Rector\Class_\FinalizeClassesWithoutChildrenRector;
use Rector\Privatization\Rector\ClassMethod\PrivatizeFinalClassMethodRector;
use Rector\Set\ValueObject\LevelSetList;
use Rector\Symfony\Set\SymfonySetList;
use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;

return static function (ContainerConfigurator $containerConfigurator): void {
    // get parameters
    $parameters = $containerConfigurator->parameters();
    $parameters->set(Option::PATHS, [
        __DIR__ . '/src',
        __DIR__ . '/tests',
    ]);
    $parameters->set(
        Option::SYMFONY_CONTAINER_XML_PATH_PARAMETER,
        __DIR__ . '/var/cache/dev/Infrastructure_KernelDevDebugContainer.xml'
    );
    $parameters->set(Option::AUTO_IMPORT_NAMES, true);
    $parameters->set(Option::SKIP, [
        RemoveRedundantDefaultClassAnnotationValuesRector::class,
        RemoveRedundantDefaultPropertyAnnotationValuesRector::class,
        RestoreDefaultNullToNullableTypePropertyRector::class,
        ClosureToArrowFunctionRector::class,
    ]);

    // Define what rule sets will be applied
    $containerConfigurator->import(LevelSetList::UP_TO_PHP_81);

    $containerConfigurator->import(DoctrineSetList::DOCTRINE_CODE_QUALITY);

    $containerConfigurator->import(SymfonySetList::SYMFONY_52);
    $containerConfigurator->import(SymfonySetList::SYMFONY_CODE_QUALITY);
    $containerConfigurator->import(SymfonySetList::SYMFONY_CONSTRUCTOR_INJECTION);

    $services = $containerConfigurator->services();

    $services->set(SplitGroupedUseImportsRector::class);
    $services->set(NameImportingPostRector::class);
    $services->set(UseAddingPostRector::class);
    $services->set(FinalizePublicClassConstantRector::class);
    $services->set(FinalizeClassesWithoutChildrenRector::class);
    $services->set(PrivatizeFinalClassMethodRector::class);
    //    $services->set(PrivatizeFinalClassPropertyRector::class);
};
